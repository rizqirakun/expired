$(document).ready(function(){
	// scroll
    $('.scroll').click(function(e) {
    	e.preventDefault();
	    $('html, body').animate({
	        scrollTop: $("#" + $(this).attr('target')).offset().top + 1
	    }, 700);
	});

   // pricing + table
	$('.table-hover tbody tr').click(function() {
		$('.table-hover tbody tr').removeClass('active');
		$(this).addClass('active');
	});

	// Countdown

	var theDate = new Date(); 
	theDate = new Date(2018, 7, 15); 
	$('#countdown').countdown({until: theDate}); 

	// accordion

});